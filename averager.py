import rospy
import math
import numpy
from std_msgs.msg import Float32 
from std_msgs.msg import Int32 
import roslaunch
incoming =[]
global x
global buffer_size

pub = rospy.Publisher('avg', Float32 , queue_size=10)


def callback(data):

    incoming.append(data.data)
    #buffer_size = rospy.get_param('~buffersize')
    buffer_size = 4
    
    
    if len(incoming) == buffer_size:
        x = 1
        average = numpy.convolve(incoming,numpy.ones(buffer_size),'valid')/buffer_size
        #average = numpy.mean(incoming)
        txt = "The average is {}"
        answer = (txt.format(average))
        rospy.loginfo(rospy.get_caller_id() + "the average is %s", average)
        incoming.pop(0)
        pub.publish(average)
      
        
        
    else:
        
       #print("Numbers not sufficient")    
        rospy.loginfo(rospy.get_caller_id() + "  Numbers not sufficient %s", data.data)
    
def listener():

    global incoming
    x = 0
    
    rospy.init_node('averager')
    rospy.Subscriber("raw", Float32, callback)
    print("no data available")
            
    
    rospy.sleep(1)
    rospy.spin()
    
    

if __name__ == '__main__':
    listener()
    
